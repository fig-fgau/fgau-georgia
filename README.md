FlightGear Australia presents Georgia

Created by Sam Fig (Fig) and Benedikt Wolf (D-ECHO) for FlightGear Flight Simulator, 2020.

To install:
Remove -master from the downloaded folder name.
Use the FlightGear launcher to add 'fgau-georgia' as an add-on scenery.

Please enjoy this scenery addon.  If you have any comments, feedback or suggestions, join the FlightGear Australia discord server, here: https://discord.gg/JzTEXsZ

With Kind Regards,
Sam and Benedikt.

---------

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 
Following sources have been used:

* SRTM-1 global elevation data from https://lpdaac.usgs.gov/products/srtmgl1v003/ 
* Landcover Data from VMAP0 by the National Geospatial-Intelligence Agency from https://gis-lab.info/qa/vmap0-eng.html
* Landcover Data from http://download.geofabrik.de and https://osmdata.openstreetmap.de/data/land-polygons.html under ODbL 1.0
* Airport Data from https://gateway.x-plane.com and manually edited by Sam Figaro and Benedikt Wolf
* Objects from FlightGear's terrasync (see scenery.flightgear.org)

Thanks to _laserman_ for scripts to process airport data files into groundnets (https://github.com/mherweg/d-laser-fgtools)!

For the developments files, please see https://github.com/D-ECHO/GeorgiaCustomScenery-dev
